package sunhapper.me.dagger2.activity;

import android.os.Bundle;

import io.reactivex.subscribers.DefaultSubscriber;
import sunhapper.me.dagger2.R;
import sunhapper.me.dagger2.bean.User;
import sunhapper.me.dagger2.commons.RxUtil;
import sunhapper.me.dagger2.mvvm.viewmodel.WanAndroidViewModel;
import timber.log.Timber;

public class WanAndroidActivity extends BaseActivity<WanAndroidViewModel> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mViewModel.login("sunhapper", "sunny5527290")
                .compose(RxUtil.<User>applySchedulers())
                .subscribe(new DefaultSubscriber<User>() {
                    @Override
                    public void onNext(User user) {
                        Timber.i(user.toString());
                    }

                    @Override
                    public void onError(Throwable t) {
                        Timber.e(t);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    protected WanAndroidViewModel createViewModel() {
        return getViewModel(WanAndroidViewModel.class);
    }

}
