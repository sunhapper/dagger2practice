package sunhapper.me.dagger2.activity;

import android.os.Bundle;

import java.util.List;

import io.reactivex.subscribers.DefaultSubscriber;
import sunhapper.me.dagger2.R;
import sunhapper.me.dagger2.bean.Meizi;
import sunhapper.me.dagger2.commons.RxUtil;
import sunhapper.me.dagger2.mvvm.viewmodel.GankMeiziViewModel;
import timber.log.Timber;

public class MainActivity extends BaseActivity<GankMeiziViewModel> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mViewModel.getMeiziList(1)
                .compose(RxUtil.<List<Meizi>>applySchedulers())
                .subscribe(new DefaultSubscriber<List<Meizi>>() {
                    @Override
                    public void onNext(List<Meizi> meizis) {
                        Timber.i("onNext: %s", meizis.size());
                    }

                    @Override
                    public void onError(Throwable t) {
                        Timber.e(t);
                    }

                    @Override
                    public void onComplete() {
                        Timber.i("onComplete");
                    }
                });
    }

    @Override
    protected GankMeiziViewModel createViewModel() {
        return getViewModel(GankMeiziViewModel.class);
    }

}
