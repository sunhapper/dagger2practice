package sunhapper.me.dagger2.di.module;

import android.arch.lifecycle.ViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import sunhapper.me.dagger2.mvvm.viewmodel.GankMeiziViewModel;
import sunhapper.me.dagger2.mvvm.viewmodel.WanAndroidViewModel;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/6 .
 */
@Module
abstract public class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(GankMeiziViewModel.class)
    abstract ViewModel bindGankMeiziViewModel(GankMeiziViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(WanAndroidViewModel.class)
    abstract ViewModel bindWanAndroidViewModel(WanAndroidViewModel viewModel);
}
