package sunhapper.me.dagger2.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import sunhapper.me.dagger2.activity.MainActivity;
import sunhapper.me.dagger2.activity.WanAndroidActivity;
import sunhapper.me.dagger2.di.scope.ActivityScope;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/8/17 .
 */
@Module
public abstract class AllActivitysModule {

    //ActivityScope 用来匹配ActivityModule中的ActivityScope
    @ActivityScope
    @ContributesAndroidInjector(modules = ActivityModule.class)
    abstract MainActivity contributeMainActivitytInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = ActivityModule.class)
    abstract WanAndroidActivity contributeWanAndroidActivitytInjector();

}