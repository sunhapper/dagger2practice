package sunhapper.me.dagger2.di.component;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;
import sunhapper.me.dagger2.app.MyApplication;
import sunhapper.me.dagger2.di.module.AllActivitysModule;
import sunhapper.me.dagger2.di.module.AppModule;
import sunhapper.me.dagger2.di.module.NetworkModule;
import sunhapper.me.dagger2.di.module.OrmModule;
import sunhapper.me.dagger2.di.module.ViewModelFactoryModule;
import sunhapper.me.dagger2.di.module.ViewModelModule;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/8/16 .
 */
@Singleton
@Component(modules = {AllActivitysModule.class, AndroidInjectionModule.class,
        AppModule.class, ViewModelFactoryModule.class, ViewModelModule.class, NetworkModule.class, OrmModule.class,
        AndroidSupportInjectionModule.class})
public interface AppComponent {
    void inject(MyApplication application);
}
