package sunhapper.me.dagger2.di.module;

import static sunhapper.me.dagger2.commons.Constants.GANK_IO_HOST;
import static sunhapper.me.dagger2.commons.Constants.WAN_ANDROID_HOST;

import android.content.Context;

import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import sunhapper.me.dagger2.api.GankApiService;
import sunhapper.me.dagger2.api.WanAndroidApiService;
import sunhapper.me.dagger2.network.LogInterceptor;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/4 .
 */
@Module
public class NetworkModule {
    private static final int DEFAULT_TIME_OUT = 15;


    @Singleton
    @Provides
    public PersistentCookieJar provideCookieJar(Context context) {
        return new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
    }

    @Singleton
    @Provides
    public OkHttpClient provideOkHttpClient(PersistentCookieJar cookieJar) {
        return new OkHttpClient.Builder()
                .connectTimeout(DEFAULT_TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(DEFAULT_TIME_OUT, TimeUnit.SECONDS)
                .addInterceptor(new LogInterceptor())
                .writeTimeout(DEFAULT_TIME_OUT, TimeUnit.SECONDS)
                .cookieJar(cookieJar)
                .build();

    }

    @Singleton
    @Provides
    public CallAdapter.Factory provideRetrofitCallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Singleton
    @Provides
    public GankApiService provideGankApiService(OkHttpClient client, CallAdapter.Factory callAdapterFactory) {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z").create();
        GsonConverterFactory factory = GsonConverterFactory.create(gson);
        return new Retrofit.Builder()
                .baseUrl(GANK_IO_HOST)
                .client(client)
                .addConverterFactory(factory)
                .addCallAdapterFactory(callAdapterFactory)
                .build().create(GankApiService.class);
    }

    @Singleton
    @Provides
    public WanAndroidApiService provideWanAndroidApiService(OkHttpClient client,
            CallAdapter.Factory callAdapterFactory) {
//        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z").create();
//        GsonConverterFactory factory = GsonConverterFactory.create(gson);
        return new Retrofit.Builder()
                .baseUrl(WAN_ANDROID_HOST)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(callAdapterFactory)
                .build().create(WanAndroidApiService.class);
    }
}
