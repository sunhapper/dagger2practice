package sunhapper.me.dagger2.di.module;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/3 .
 */
@Module
public class AppModule {
    private Application app;

    public AppModule(Application app) {
        this.app = app;
    }

    @Provides
    Gson provideGson() {
        return new Gson();
    }

    @Singleton
    @Provides
    public Application provideApp() {
        return app;
    }

    @Singleton
    @Provides
    public Context provideContext() {
        return app;
    }


}
