package sunhapper.me.dagger2.di.module;

import dagger.Module;
import dagger.Provides;
import sunhapper.me.dagger2.bean.User;
import sunhapper.me.dagger2.di.scope.UserScope;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/12 .
 */
@Module
public class UserModule {
    private User user;

    public UserModule(User user) {
        this.user = user;
    }

    @Provides
    @UserScope
    User provideUser() {
        return user;
    }

}
