package sunhapper.me.dagger2.di.module;

import android.app.Application;
import android.arch.persistence.room.Room;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import sunhapper.me.dagger2.database.AppDatabase;
import sunhapper.me.dagger2.mvvm.datasource.MeiziDao;
import sunhapper.me.dagger2.mvvm.datasource.UserDao;


/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/5 .
 */
@Module
public class OrmModule {
    public static final String DB_NAME = "dagger_practice";

    @Provides
    @Singleton
    public AppDatabase provideAppDatabase(Application application) {
        return Room.databaseBuilder(application, AppDatabase.class, DB_NAME).build();
    }

    @Provides
    public MeiziDao provideMeiziDao(AppDatabase appDatabase) {
        return appDatabase.meiziDao();
    }

    @Provides
    public UserDao provideUserDao(AppDatabase appDatabase) {
        return appDatabase.userDao();
    }

}
