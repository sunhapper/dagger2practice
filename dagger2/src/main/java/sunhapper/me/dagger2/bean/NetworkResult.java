package sunhapper.me.dagger2.bean;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/12 .
 */
public interface NetworkResult<T> {
    T get();

    boolean isSuccess();

    String getErrorMessage();

}
