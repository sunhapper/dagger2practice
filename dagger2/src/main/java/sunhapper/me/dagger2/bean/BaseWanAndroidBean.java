package sunhapper.me.dagger2.bean;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/5 .
 */
public class BaseWanAndroidBean<T> implements NetworkResult<T>{
    public int errorCode;
    public String errorMsg;
    public T data;

    @Override
    public T get() {
        return data;
    }

    public boolean isSuccess() {
        return errorCode == 0;
    }

    @Override
    public String getErrorMessage() {
        return errorMsg;
    }

}
