package sunhapper.me.dagger2.bean;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.List;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/12 .
 */
@Entity
public class User {

    public String email;
    public String icon;
    @PrimaryKey
    @NonNull
    public int id;
    public String password;
    public int type;
    public String username;
    @Ignore
    public List<Integer> collectIds;

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", icon='" + icon + '\'' +
                ", id=" + id +
                ", password='" + password + '\'' +
                ", type=" + type +
                ", username='" + username + '\'' +
                '}';
    }
}
