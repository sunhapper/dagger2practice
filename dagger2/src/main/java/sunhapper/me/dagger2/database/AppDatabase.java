package sunhapper.me.dagger2.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import sunhapper.me.dagger2.bean.Meizi;
import sunhapper.me.dagger2.bean.User;
import sunhapper.me.dagger2.mvvm.datasource.MeiziDao;
import sunhapper.me.dagger2.mvvm.datasource.UserDao;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/6 .
 */
@Database(entities = {Meizi.class, User.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract MeiziDao meiziDao();

    public abstract UserDao userDao();
}
