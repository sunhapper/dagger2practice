package sunhapper.me.dagger2.app;

import android.app.Activity;
import android.app.Application;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import sunhapper.me.dagger2.BuildConfig;
import sunhapper.me.dagger2.di.component.AppComponent;
import sunhapper.me.dagger2.di.component.DaggerAppComponent;
import sunhapper.me.dagger2.di.module.AppModule;
import timber.log.Timber;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/8/16 .
 */
public class MyApplication extends Application implements HasActivityInjector {
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingActivityInjector;
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        appComponent.inject(this);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingActivityInjector;
    }
}
