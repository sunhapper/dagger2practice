package sunhapper.me.dagger2.commons;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/5 .
 */
public class Constants {
    public static final String GANK_IO_HOST = "http://gank.io/";
    public static final String WAN_ANDROID_HOST = "http://www.wanandroid.com/";
}
