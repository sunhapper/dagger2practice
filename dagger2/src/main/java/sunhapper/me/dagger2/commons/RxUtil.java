package sunhapper.me.dagger2.commons;

import org.reactivestreams.Publisher;

import io.reactivex.Flowable;
import io.reactivex.FlowableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import sunhapper.me.dagger2.bean.BaseGankBean;
import sunhapper.me.dagger2.bean.NetworkResult;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/6 .
 */
public class RxUtil {
    private RxUtil() {
    }

    public static <T> FlowableTransformer<BaseGankBean<T>, T> getGankResultContent() {
        return new FlowableTransformer<BaseGankBean<T>, T>() {
            @Override
            public Publisher<T> apply(Flowable<BaseGankBean<T>> upstream) {
                return upstream.map(new Function<BaseGankBean<T>, T>() {
                    @Override
                    public T apply(BaseGankBean<T> listBaseGankBean) throws Exception {
                        return listBaseGankBean.results;
                    }
                });
            }
        };
    }

    public static <Upstream> FlowableTransformer<Upstream, Upstream> applySchedulers() {
        return new FlowableTransformer<Upstream, Upstream>() {
            @Override
            public Publisher apply(Flowable upstream) {
                return upstream.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            }
        };
    }

    public static <T> Function<NetworkResult<T>, Publisher<T>> getRxNetworkErrorFun() {
        return new Function<NetworkResult<T>, Publisher<T>>() {
            @Override
            public Publisher<T> apply(NetworkResult<T> tNetworkResult) throws Exception {
                if (tNetworkResult.isSuccess()) {
                    return Flowable.just(tNetworkResult.get());
                } else {
                    return Flowable.error(new Throwable(tNetworkResult.getErrorMessage()));
                }
            }
        };
    }
}
