package sunhapper.me.dagger2.commons;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/12 .
 */
@Singleton
public class SharedPreferenceHelper {
    private static final String APP_NAME = "DaggerPractice";
    private static final String SP_KEY_LOGIN_USER_ID = "sp_key_login_user_id";
    public static final int UNLOGIN_USER_ID = -1001;
    private SharedPreferences mSharedPreferences;

    @Inject
    public SharedPreferenceHelper(Application mApplication) {
        mSharedPreferences = mApplication.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);
    }

    public void putString(String k, String v) {
        mSharedPreferences.edit().putString(k, v).apply();
    }

    public String getString(String k) {
        return mSharedPreferences.getString(k, "");
    }

    public void putInt(String k, int v) {
        mSharedPreferences.edit().putInt(k, v).apply();
    }

    public int getInt(String k) {
        return mSharedPreferences.getInt(k, UNLOGIN_USER_ID);
    }

    public void logout() {
        putLoginUserId(UNLOGIN_USER_ID);
    }

    public void putLoginUserId(int id) {
        putInt(SP_KEY_LOGIN_USER_ID, id);
    }

    public int getLoginUserId() {
        return getInt(SP_KEY_LOGIN_USER_ID);

    }

}
