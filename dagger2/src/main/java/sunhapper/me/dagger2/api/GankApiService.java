package sunhapper.me.dagger2.api;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import sunhapper.me.dagger2.bean.BaseGankBean;
import sunhapper.me.dagger2.bean.Meizi;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/4 .
 */
public interface GankApiService {
    @GET("api/data/福利/{pageSize}/{pageNum}")
    Flowable<BaseGankBean<List<Meizi>>> getMeiziImgList(@Path("pageSize") int pageSize,@Path("pageNum") int pageNum);
}
