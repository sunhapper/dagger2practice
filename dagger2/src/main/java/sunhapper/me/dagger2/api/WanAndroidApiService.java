package sunhapper.me.dagger2.api;

import io.reactivex.Flowable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import sunhapper.me.dagger2.bean.BaseWanAndroidBean;
import sunhapper.me.dagger2.bean.User;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/4 .
 */
public interface WanAndroidApiService {
    /**
     * 登录
     */
    @FormUrlEncoded
    @POST("user/login")
    Flowable<BaseWanAndroidBean<User>> login(@Field("username") String userName, @Field("password") String password);

    /**
     * 注册
     */
    @FormUrlEncoded
    @POST("user/register")
    Flowable<BaseWanAndroidBean<User>> register(@Field("username") String username, @Field("password") String password,
            @Field("repassword") String repassword);

    /**
     * 退出
     */
    @FormUrlEncoded
    @GET("user/logout/json")
    void logout();
}
