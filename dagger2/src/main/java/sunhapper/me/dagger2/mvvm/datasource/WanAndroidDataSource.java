package sunhapper.me.dagger2.mvvm.datasource;

import javax.inject.Inject;

import io.reactivex.Flowable;
import sunhapper.me.dagger2.bean.User;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/12 .
 */
public class WanAndroidDataSource {

    @Inject
    public WanAndroidDataSource() {
    }

    @Inject
    UserDao mUserDao;


    public Flowable<User> getUserFlowabel(int id) {
        return mUserDao.getUserFlowabel(id);
    }

    public User getUser(int id) {
        return mUserDao.getUser(id);
    }

    public void insertOrReplaceUser(User user) {
        if (user == null) {
            return;
        }
        mUserDao.insertOrReplaceUser(user);
    }
}
