package sunhapper.me.dagger2.mvvm.datasource;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import io.reactivex.Flowable;
import sunhapper.me.dagger2.bean.User;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/6 .
 */
@Dao
public interface UserDao {

    @Query("SELECT * from User WHERE id=:id")
    Flowable<User> getUserFlowabel(int id);

    @Query("SELECT * from User WHERE id=:id")
    User getUser(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOrReplaceUser(User user);
}
