package sunhapper.me.dagger2.mvvm.repository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import sunhapper.me.dagger2.api.GankApiService;
import sunhapper.me.dagger2.bean.BaseGankBean;
import sunhapper.me.dagger2.bean.Meizi;
import sunhapper.me.dagger2.commons.RxUtil;
import sunhapper.me.dagger2.mvvm.datasource.GankDataSource;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/5 .
 */
public class GankRepository {
    public static final int DEFAULT_PAGE_SIZE = 30;
    public static final int INIT_PAGE_NUM = 1;
    @Inject
    GankApiService mGankApiService;
    @Inject
    GankDataSource mGankDataSource;

    @Inject
    public GankRepository() {
    }

    public Flowable<List<Meizi>> getMeiziList(int pageNum) {
        Flowable<List<Meizi>> flowable;
        final Flowable<BaseGankBean<List<Meizi>>> networkFlowable = mGankApiService.getMeiziImgList(DEFAULT_PAGE_SIZE,
                pageNum);
        if (pageNum == INIT_PAGE_NUM) {

            Single<List<Meizi>> cacheSingle = mGankDataSource.getCacheMeizi(DEFAULT_PAGE_SIZE);
            flowable = Flowable.concatArrayEager(cacheSingle.toFlowable(),
                    networkFlowable.compose(RxUtil.<List<Meizi>>getGankResultContent()).doOnNext(
                            new Consumer<List<Meizi>>() {
                                @Override
                                public void accept(List<Meizi> meizis) throws Exception {
                                    mGankDataSource.cacheMeizi(meizis);
                                }
                            }));

        } else {
            flowable = networkFlowable.compose(RxUtil.<List<Meizi>>getGankResultContent());
        }
        return flowable;
    }
}
