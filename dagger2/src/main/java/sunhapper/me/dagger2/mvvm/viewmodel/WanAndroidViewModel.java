package sunhapper.me.dagger2.mvvm.viewmodel;

import android.app.Application;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import io.reactivex.Flowable;
import sunhapper.me.dagger2.bean.User;
import sunhapper.me.dagger2.mvvm.repository.WanAndroidRepository;

/**
 * Created by sunhapper(haipeng.sun3@56qq.com) on 2018/9/6 .
 */
public class WanAndroidViewModel extends BaseViewModel {
    @Inject
    WanAndroidRepository mWanAndroidRepository;

    @Inject
    public WanAndroidViewModel(@NonNull Application application) {
        super(application);
    }

    public Flowable<User> login(String userName, String pwd) {
        return mWanAndroidRepository.login(userName, pwd);
    }

    public User getLoginUserInfo() {
        return mWanAndroidRepository.getLoginUserInfo();
    }

    public User getLocalUserInfo(int userId) {
        return mWanAndroidRepository.getLocalUserInfo(userId);
    }

    public boolean isLogin() {
        return mWanAndroidRepository.isLogin();
    }
}
